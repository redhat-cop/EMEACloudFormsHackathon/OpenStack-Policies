# OpenStack policy for CloudForms

## Problem description

The CloudForms operator would like to create a role in OpenStack so that it just
has the right to access the OSP API calls that are called by CloudForms.

## Where to find the file policies

Currently, in OpenStack, default policies are stored in two different locations:

1. In code: Nova, Ironic and Keystone
2. In the policy.json: Neutron, Glance, Heat, Ceilometer, Aodh, Panko, Gnocchi

## How to generate the default policy files for the projects that store it code

As we will need to change plenty of default policies, we will need to generate
the policy files for Nova, Ironic and Keystone projects. [1]

This is the command you would use it for generating the Nova policy file [3]:

```
oslopolicy-policy-generator --namespace nova | \
     python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=4)' \
     > nova-policy.json
```
## How to create the cloudforms role
In any host with the openstack clients installed and able to access to the APIS.
Note that we are using role assignment inheritance. With this role assignment, the CloudForms Management Engine instance can perform all operations on all projects within the default domain.

```
openstack role create cloudforms
openstack project create cloudforms
openstack user create cloudforms --password cloudforms --project cloudforms
openstack role add --user cloudforms cloudforms --project cloudforms
openstack role add --user cloudforms _member_ --project cloudforms
openstack role add --user cloudforms --user-domain Default --project Default --project-domain Default --inherited cloudforms
openstack role add --user cloudforms --user-domain Default --project Default --project-domain Default --inherited admin
```
## How to load these policies
In every OpenStack controller:
```
cp policies/nova-policy.json /etc/nova/policy.json
cp policies/glance-policy.json /etc/glance/policy.json
```

# References
[1]. https://docs.openstack.org/oslo.policy/1.17.0/usage.html
[2]. https://adam.younglogic.com/2018/02/openstack-hmt-cloudforms/
[3]. https://access.redhat.com/solutions/3182981  
